package com.example.assignmenteleven

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Cat(val imageResource:Int,val catBreed:String,val description:String): Parcelable{
}