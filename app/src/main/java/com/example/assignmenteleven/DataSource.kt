package com.example.assignmenteleven

class DataSource {
    companion object
    {
        fun loadData():MutableList<Cat>
        {
            return mutableListOf(
                Cat(R.mipmap.abyssinian_cat,"abyssinian cat","cat1"),
                Cat(R.mipmap.australian_mist,"australian mist","cat2"),
                Cat(R.mipmap.birman,"birman","cat3"),
                Cat(R.mipmap.british_shorthair,"british shorthair","cat4"),
                Cat(R.mipmap.japanese_bobtail,"japanese bobtail","cat5"),
                Cat(R.mipmap.la_perm,"la perm","cat6"),
                Cat(R.mipmap.siberian_cat,"siberian cat","cat7"),
                Cat(R.mipmap.persian_cat,"persian cat","cat8"),
                Cat(R.mipmap.oriental_shorthair,"oriental shorthair","cat9"),
                Cat(R.mipmap.abyssinian_cat,"abyssinian cat","cat1"),
                Cat(R.mipmap.australian_mist,"australian mist","cat2"),
                Cat(R.mipmap.birman,"birman","cat3"),
                Cat(R.mipmap.british_shorthair,"british shorthair","cat4")



            )
        }
    }
}