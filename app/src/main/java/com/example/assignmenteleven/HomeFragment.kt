package com.example.assignmenteleven

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import com.example.assignmenteleven.adapter.CatsGalleryAdapter
import com.example.assignmenteleven.databinding.FragmentHomeBinding
import android.R
import androidx.navigation.fragment.findNavController


class HomeFragment : Fragment() {

    private var binding: FragmentHomeBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(inflater,container,false)

        setUpRecyclerView()


        return binding?.root

    }

    fun setUpRecyclerView()
    {
        binding?.rvCatGallery?.layoutManager = GridLayoutManager(activity,3)
        val adapter = CatsGalleryAdapter()
        binding?.rvCatGallery?.adapter = adapter


        adapter.callback = object : CatsGalleryAdapter.onItemClickListener
        {

            override fun onItemClick(position: Int) {
                val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(DataSource.loadData()[position])
                findNavController().navigate(action)
            }

        }

        adapter.addData(DataSource.loadData())
    }



    /*fun setUpSearchView(adapter:CatsGalleryAdapter)
    {
        binding?.svSearch?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                adapter.filter.filter(newText)
                return false
            }
        })
    } */

}