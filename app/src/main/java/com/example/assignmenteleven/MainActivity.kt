package com.example.assignmenteleven

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.assignmenteleven.adapter.CatsGalleryAdapter
import com.example.assignmenteleven.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_AssignmentEleven)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


}